package astar;

public class Edge {
	private Node dest;
	private int cost;
	
	public Edge(Node dest,int cost){
            this.setDest(dest);
            this.setCost(cost);
	}

	public Node getDest() {
		return dest;
	}

	public void setDest(Node dest) {
		this.dest = dest;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}
}
