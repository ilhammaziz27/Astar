package astar;

import java.util.ArrayList;

public class Map {
    private final ArrayList<Node> nodes;
    private final ArrayList<Node> listOpen;
    private final ArrayList<Node> listClosed;

    public Map(){
        this.nodes=new ArrayList<>();
        this.listOpen=new ArrayList<>();
        this.listClosed=new ArrayList<>();
    }

    public void init(){
        Node ravenna=new Node("Ravenna", 0);
        Node rimini=new Node("Rimini", 0.5);
        Node ferrara=new Node("Ferrara", 5);
        Node forli=new Node("Forli", 2);
        Node cesena=new Node("Cesena", 4.5);
        Node faenza=new Node("Faenza", 4);
        Node imola=new Node("Imola", 5);
        Node emilia=new Node("Emilia", 6);
        Node terme=new Node("Terme", 7);
        Node carpi=new Node("Carpi", 8);
        Node piacenza=new Node("Piacenza", 10);
        Node bobbia=new Node("Bobbia", 10.5);

        bobbia.addDestination(piacenza, 5);
        bobbia.addDestination(terme, 3);
        bobbia.addDestination(cesena, 15);

        piacenza.addDestination(carpi, 3);
        piacenza.addDestination(terme, 3);

        terme.addDestination(emilia, 2);
        terme.addDestination(faenza, 8);

        carpi.addDestination(ferrara, 8);
        carpi.addDestination(emilia, 2);

        emilia.addDestination(imola, 2);

        ferrara.addDestination(imola, 3);
        ferrara.addDestination(ravenna, 6);

        imola.addDestination(faenza, 1);
        imola.addDestination(forli, 3);

        faenza.addDestination(forli, 2);
        faenza.addDestination(cesena, 6);

        forli.addDestination(cesena, 2);
        forli.addDestination(ravenna, 3);

        cesena.addDestination(rimini, 5);

        rimini.addDestination(ravenna, 1);

        this.nodes.add(ravenna);
        this.nodes.add(rimini);
        this.nodes.add(ferrara);
        this.nodes.add(forli);
        this.nodes.add(cesena);
        this.nodes.add(imola);
        this.nodes.add(emilia);
        this.nodes.add(terme);
        this.nodes.add(carpi);
        this.nodes.add(piacenza);
        this.nodes.add(bobbia);
        System.out.println("Map initialized");

        doAStar(bobbia, ravenna);
    }

    public void showGraph(){
        for (Node tempNode : this.nodes) {
            System.out.printf(tempNode.getLabel()+" : \t");

            for (Edge dests : tempNode.getDests()) {
                    System.out.printf(dests.getDest().getLabel()+"("+dests.getCost()+"), ");
            }
            System.out.println();
        }
    }

    public boolean isOpen(Node dest){
        for (Node node : listOpen) {
            if(dest==node){
                return true;
            }
        }

        return false;
    }

    public boolean isClosed(Node dest){
            for (Node node : listClosed) {
                    if(dest==node){
                            return true;
                    }
            }

            return false;
    }

    public void printList(ArrayList<Node> nodes){
            System.out.println("----------------------");
            System.out.println("Node    \tF(n)");
            System.out.println("----------------------");
            for (Node node : nodes) {
                    System.out.printf(node.getLabel()+"     \t"+node.getfVal());
                    System.out.println("");
            }
            System.out.println();
    }
    
    Node getLowestCost(){
        if(listOpen.isEmpty())
            return null;

        Node temp=listOpen.get(0);

        for (Node node : listOpen) {
            node.setfVal(node.getgVal()+node.gethVal());
            System.out.println(node.getgVal()+" + "+node.gethVal());
            if(node.getfVal()<temp.getfVal()){
                temp=node;
            }
        }

        return temp;
    }	

    public void doAStar(Node start, Node goal){
            double tempCost=0;
            Node current=new Node();

            start.setfVal(start.gethVal());
            listOpen.add(start);
            
            int n=0;
            while(!this.listOpen.isEmpty()){
                current=getLowestCost();
                System.out.println("=============================================");
                System.out.println("Iteration " + n++);
                System.out.println("OPEN      : "); printList(listOpen);
                System.out.println("CLOSED    : "); printList(listClosed);
                //System.out.println("F(n)    : " + tempCost);
                System.out.println("=============================================");

                if(current==goal)
                    break;

                listOpen.remove(current);

                for (Edge dest : current.getDests()) {
                        tempCost=current.getgVal() + dest.getCost() + dest.getDest().gethVal();

                        if(isOpen(dest.getDest())){
                                if(dest.getDest().getgVal()<=tempCost)
                                        continue;
                        }
                        else if(isClosed(dest.getDest())){
                                if(dest.getDest().getgVal()<=tempCost){
                                        listOpen.add(dest.getDest());
                                        listClosed.remove(dest.getDest());
                                }
                        }
                        else{
                                listOpen.add(dest.getDest());
                        }

                        dest.getDest().setgVal(tempCost);
                        dest.getDest().setParent(current);

                }
                listClosed.add(current);
            }

            if(current==goal){
                    System.out.println("Solution found!");
                    showPath(start, goal);
            }
            else{
                    System.out.println("Solution not found!");
            }
    }

    public void showPath(Node startNode, Node goalNode){
            Node current=goalNode;
            ArrayList<Node> temp=new ArrayList<>();

            System.out.println("Best path");
            while(current!=null){
                    temp.add(current);
                    current=current.getParent();
            }

            for(int i=temp.size()-1;i>=0;i--){
                    System.out.print(temp.get(i).getLabel()+" ");
            }
    }

    public static void main(String[] args){
            Map app=new Map();
            app.init();
    }
}