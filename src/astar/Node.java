package astar;

import java.util.ArrayList;

public final class Node {
    private ArrayList<Edge> dests;
    private double hVal;
    private String label;
    private Node parent;
    private double fVal;
    private double gVal;

    public Node(){
        this.setDests(new ArrayList<>());
    }

    public Node(String label, double hVal){
        this.label=label;
        this.hVal=hVal;
        this.setDests(new ArrayList<>());
        this.gVal=0;
        this.fVal=0;
    }

    public void addDestination(Node dest, int cost){
        Edge newDest=new Edge(dest,cost);
        this.getDests().add(newDest);
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public double gethVal() {
        return hVal;
    }

    public void sethVal(double hVal) {
        this.hVal = hVal;
    }

    public ArrayList<Edge> getDests() {
        return dests;
    }

    public void setDests(ArrayList<Edge> dests) {
        this.dests = dests;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public double getfVal() {
        return fVal;
    }

    public void setfVal(double fVal) {
        this.fVal = fVal;
    }

    public double getgVal() {
            return gVal;
    }

    public void setgVal(double gVal) {
        this.gVal = gVal;
    }
}
